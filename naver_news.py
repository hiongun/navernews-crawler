#!/bin/env python
# -*- coding: utf-8 -*-

import sys, time, os

def DEBUG_PRINT(str_line):
#{
    import inspect
    print("(%s,%d) %s" % (__file__, inspect.stack()[1][2], str_line.strip()))
#}

def get_url_in(url, header = None):
#{
    import requests

    if header is None:
        header = {
            "User-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
            "Referer": url,
        }

    delay = 1
    while (delay < 30):
        try:
            r = requests.get(url, headers = header)
            break
        except:
            DEBUG_PRINT("FAIL get")
            time.sleep(delay)
            delay *= 2

    # DEBUG_PRINT("%s" % r.content)

    encoding = "utf-8"
    lines = str(r.content).split("\n")
    for idx, line in enumerate(lines):
        if idx >= 100: break
        if line.find("charset=\"euc-kr\"") > 0:
            encoding = "euc-kr"
            break

    # 인코딩이 잘 맞지 않는 문자는 ignore/replace한다
    try:
        resp_body = r.content.decode(encoding, 'ignore')
    except:
        DEBUG_PRINT("FAIL decode")

    # DEBUG_PRINT("resp_body: %s" % (resp_body))

    return resp_body
#}

def get_url(url, header=None):
#{
    # 일시적인 페이지 접속 오류를 잡아준다
    html = get_url_in(url, header)

    return html
#}

def strip_useless_chars(txt):
#{
    txt = txt.replace("\"", " ").replace("\'", " ")
    txt = txt.replace(",", " ")
    txt = txt.replace("\n", " ").replace("\r", " ").replace("\t", " ")
    txt = txt.replace("&#35;", "#").replace("&#035;", "#")
    txt = txt.replace("&#39;", "...").replace("&#039;", "...")
    txt = txt.replace("&#169;", "©")
    txt = txt.replace("&#160;", " ")
    txt = txt.replace("&ldquo;", "“").replace("&rdquo;", "”")
    txt = txt.replace("&lsquo;", "`").replace("&rsquo;","’")
    txt = txt.replace("&quot;", "”").replace("&#34;", " ").replace("&#034;", " ")
    txt = txt.replace("&middot;", "·")
    txt = txt.replace("&lt;", "<").replace("&gt;", ">")
    txt = txt.replace("&amp;", "&").replace("&nbsp;", " ")
    txt = txt.replace("\\n", " ")
    txt = txt.replace("      ", " ")
    txt = txt.replace("   ", " ")
    txt = txt.replace("  ", " ")
    txt = txt.strip()

    return txt
#}

def strip_tag(text, begin_tag, end_tag):
#{
    p = text.find(begin_tag)
    while (p >= 0):
        r = text[p:].find(end_tag)
        if (p >= 0 and r >= 0):
            text = text[0:p] + text[p+r+len(end_tag): ]
        else:
            break
        p = text.find(begin_tag)
    return text
#}

def strip_links(text):
#{
    import re

    # <a href= ... _blank">▶..</a> 패턴 제거하기
    # <a href="https://..." target="_blank">▶기사제보 및 문의 </a>

    p = re.compile(r'<a href=[^>]+_blank.>▶[^<]+</a>')
    m = p.search(text)
    DEBUG_PRINT("m: %s" %(m))
    while m != None:
        DEBUG_PRINT("matched: [%s]" % (m.group()))
        # 중간 매칭 부분을 잘라 낸다
        text = text[0:m.start()] + text[m.end(): ]
        m = p.search(text)

    # 패턴 제거하기
    # [<a href="https://..." target="_blank">▶기사제보 및 문의 </a>]

    p = re.compile(r'\[<a href=[^>]+_blank.>[^<]+</a>\]')
    m = p.search(text)
    DEBUG_PRINT("m: %s" %(m))
    while m != None:
        DEBUG_PRINT("matched: [%s]" % (m.group()))
        # 중간 매칭 부분을 잘라 낸다
        text = text[0:m.start()] + text[m.end(): ]
        m = p.search(text)

    return text
#}


def strip_useless_tags(body_text):
#{
    # 기사 본문에서, 불필요한 태그 제거하기
    # 네이버 신문기사 페이지의 태그에 따라 해킹이 많이 필요함

    body_text = strip_tag(body_text, "<!-- TV플레이어 -->", "<!-- // TV플레이어 -->")
    body_text = strip_tag(body_text, "<script ", "</script>")


    body_text = strip_tag(body_text, "<strong", ">")
    body_text = strip_tag(body_text, "<font ", ">")
    body_text = strip_tag(body_text, "<FONT ", ">")

    body_text = strip_links(body_text)

    body_text = strip_tag(body_text, "<a ", ">")
 
    body_text = strip_tag(body_text, "<iframe ", ">")

    body_text = strip_tag(body_text, "<ul", "</ul>")
    body_text = strip_tag(body_text, "<script", "</script>")
    body_text = strip_tag(body_text, "<span ", "</span>")
    body_text = strip_tag(body_text, "<table ", "</table>")
    body_text = strip_tag(body_text, "<TABLE ", "</TABLE>")

    body_text = strip_tag(body_text, "<!--", "-->")
    body_text = strip_tag(body_text, "<div ", ">")

    body_text = strip_tag(body_text, "<h4 ", ">")

    remove_tags = ["<!-- 본문 내용 -->", "<!-- // 본문 내용 -->", "<br>", "<br/>", "<br />",
        "</font>", "<b>", "</b>", "</br>", "<p>", "</p>", "<strong>", "</strong>", "</FONT>",
        "</a>", "</iframe>", "</TD>", "</TR>", "</TABLE>", "</div>", "</h4>",
        "<center>", "</center>", "<font>", "<span>", "</span>", "</B>", "<B>", "<STRONG>", "</STRONG>",
        "<ul>", "</ul>",
        "▶ 뉴스는 머니투데이",
        "▶뉴스는 머니투데이", ]

    for remove_tag in remove_tags:
        body_text = body_text.replace(remove_tag, " ")

    ending_strings = [" 네이버에서 이데일리", " ▶연합뉴스 채널 ", " ▶ 중앙일보 홈페이지 ",
        " ▶ 최신 뉴스 ▶", " ▶연합뉴스 채널 ", " ▶ 뉴시스 빅데이터 ", " 저작권자 ⓒ 서울경제 ", 
        " 저작권자 © CBS ", " 저작권자 © CBS ", " ▶ [ 크립토허브 ] ", " ▶ 네이버 홈에서",
        " [네이버 메인에서", " GoodNews paper ", " <저작권자", " ＜ⓒ",
        " [신뢰도 1위 ‘한겨레’",
        " ▶ 확 달라진 노컷뉴스 ", " ▶ 동아일보 단독 ",
        " ▶ 중앙일보 홈페이지 ", " [ⓒ 매일경제",
        " ▶매경 뉴스레터", " ▶[이벤트] ", "▶ 최신 뉴스 ▶",
        " 디지털타임스 홈페이지 ", " ▶ 경향신문 ",
        " ⓒ매일신문 - ", " ⓒ (주)데일리안",
        " - Copyrights ⓒ 헤럴드", " ▶ 전자신문 인기 포스트",
        "IT언론의 새로운 대안[디지털데일리]",
        "[ⓒ 아이뉴스24", "▶아이뉴스TV",
        "[저작권자 (주)블로터앤미디어",
        "[저작권자(c) MBC",
        "ⓒ 세상을 보는 눈",
        " ▶헤럴드경제 채널", " ▶ 경향신문 ",
        " ©경향신문 ", " [저작권자(c) YTN ",
        " ▶ 네이버메인에", " [© 뉴스1코리아",
        " © SBS ", " ▶ 최신 뉴스 ", " ▶ KBS뉴스와 ",
        " ▶ 한겨레 절친이 ", " ▶헤럴드경제 채널 설정",
        " ▶ 채널 설정", " ▶ KBS뉴스와 친구",
        " ▶기사제보 및 문의", " [저작권자ⓒ ",
        "☞ 내가 보낸 영상이 SBS 뉴스로",
        "※ ⓒ SBS & SBS",
        " ▶ 재미있는 세상[나우뉴스" ]
        
    for ending in ending_strings:
        p = body_text.find(ending)
        if p > 0: body_text = body_text[0: p]
 
    body_text = strip_tag(body_text, "<!-- SUB_TITLE_START-->", "<!-- SUB_TITLE_END-->")

    body_text = body_text.replace("\t", " ")
    body_text = body_text.replace("    ", " ")
    body_text = body_text.replace("   ", " ")
    body_text = body_text.replace("  ", " ")

    body_text = body_text.replace(",", " ")

    return body_text
#}

def meta_extract_content(key, line):
#{
    # DEBUG_PRINT("extract_content: %s" % (line))

    # 다음과 같은 코드에서, content값을 파싱해 준다.
    # <meta property="og:type" content=" "/>

    if line.find(key) < 0:
        return ''

    p = line.lower().find("content=")
    if p > 0:
        line = line[p+len("content="):]

    q = line.find("/>")
    if q > 0:
        line = line[0:q]

    line = line.replace("\"", " ") 
    line = line.replace("\'", " ") 

    line = line.strip()

    # DEBUG_PRINT("extract_content returns: %s" % (line))

    return line
#}

def get_oid_aid_from_url(url):
#{
    # URL에서 OID, AID 추출

    oid = ''
    aid = ''
    kvs = url[url.find('?'): ].split("&")
    for kv in kvs:
        k_v = kv.split("=")
        if len(k_v) == 2:
            if k_v[0] == 'oid':
                oid = k_v[1]
            if k_v[0] == 'aid':
                aid = k_v[1]

        # oid = url.split("oid=")[1].split("&")[0]
        # aid = url.split("aid=")[1]

    # DEBUG_PRINT("oid: %s" % (oid))
    # DEBUG_PRINT("aid: %s" % (aid))

    return (oid, aid)
#}


def get_comments(url):
#{

    # 네이버 뉴스 url을 입력합니다.
    # url="https://news.naver.com/main/read.nhn?mode=LSD&mid=sec&sid1=100&oid=020&aid=0003214179"

    # https://news.naver.com/main/read.nhn?mode=LSD&mid=sec&sid1=110&oid=032&aid=0002914610

    comment_list = []

    # URL에서 OID, AID 추출

    (oid, aid) = get_oid_aid_from_url(url)

    header = {
        "User-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
        "Referer": url,
    }

    for page in range(1, 200):

        DEBUG_PRINT("page: %d" % (page))

        c_url = "https://apis.naver.com/commentBox/cbox/web_neo_list_jsonp.json?ticket=news&templateId=default_society&pool=cbox5&_callback=jQuery1707138182064460843_1523512042464&lang=ko&country=&objectId=news" + oid + "%2C" + aid + "&categoryId=&pageSize=20&indexSize=10&groupId=&listType=OBJECT&pageType=more&page=" + str(page) + "&refresh=false&sort=FAVORITE"

        DEBUG_PRINT("c_url: %s" % (c_url))

        html = get_url(c_url, header)

        added = False

        # 'comments', .. "," .. 'userIdNo', 'modTime', .. "," .. 'modTimeGmt'
        while (0 < len(html)):
            contents = ''
            modTime = ''
            p = html.find("\"contents\":\"")
            if p > 0:
                # DEBUG_PRINT("p: %s" % (html[p:]))
                html = html[p+len("\"contents\":\""):]
                q = html.find("\",\"")
                contents = html[0:q]
                html = html[q:]

            # DEBUG_PRINT("contents: %s" % (contents))

            p = html.find("\"modTime\":\"")
            if p > 0:
                html = html[p+len("\"modTime\":\""):]
                q = html.find("\",\"")
                modTime = html[0:q]
                resp_body = html[q:]

                p = modTime.find("+")
                if p > 0: modTime = modTime[0: p] 
                p = modTime.find(".")
                if p > 0: modTime = modTime[0: p] 

                modTime = modTime.replace("T", "")
                modTime = modTime.replace(":", "")
                modTime = modTime.replace("-", "")

            # DEBUG_PRINT("modTime: %s" % (modTime))

            if contents == "" or modTime == "":
                break

            # uid = oid + "." + aid

            contents = strip_useless_chars(contents)

            comment_list.append([modTime, contents])
            added = True

        if not added:
            break
 
    return comment_list
#}


# 네이버의 특정 날짜(ymd_i), 섹션(section_id_1)의 기사를 가져오기
def crawl_naver_news_date_section(ymd_i, section_id_1, section_name, fp, uids_crawled):
#{
    article_urls = []
    oids = {}

    for page in range(1, 150): # 해당 섹션, 날짜의 페이지별 처리
    #{
        # url = "http://news.naver.com/main/list.nhn?mode=LSD&mid=shm&sid1=%s&date=%s&page=%d" % (section_id_1, ymd_i, page)
        url = "http://news.naver.com/main/list.nhn?sid1=%s&date=%s&page=%d" % (section_id_1, ymd_i, page)
        DEBUG_PRINT("section/ymd/page URL: %s" % (url))

        html = get_url(url)

        # 기사본문 URL 패턴을 찾아서 기사 번호 중복이 아닌 새로운 기사 URL이 나오면 article_urls에 기억
        new_articles = False # 특정 페이지(page: 0 ... 40)에서 새로운 기사의 URL이 나왔는가?
        for line in html.split("\n"):
            # <a href="http://news.naver.com/main/read.nhn?mode=LSD&mid=shm&sid1=103&oid=449&aid=0000148930" class="nclicks(fls.
            # DEBUG_PRINT(line)

            if line.find("aid") < 0: continue
            if line.find("nclicks(fls.") < 0: continue
            if line.find("a href") < 0: continue
            if line.find("ranking") >= 0: continue
            if line.find("sports.news") >= 0: continue

            url = line[line.find("<a href=")+9:line.find("\" class=\"nclick")] # URL
            oid = url[url.find("oid="): ] # 기사번호
            if not oid in oids: # not oids.has_key(oid)
                oids[oid] = 1
                new_articles = True
                article_urls.append(url)
            # DEBUG_PRINT("url: %s, oid:%s" % (url, oid))

        # DEBUG_PRINT( html )

        # 새로운 기사가 없으면, 페이지 번호를 그만 증가시키고 종료
        if not new_articles:
            break

        if ((page+1) % 10 == 0): time.sleep(0.1)
    #}


    # 해당 날짜, 해당 섹선에서 몇개의 기사 URL이 발견되었나?
    DEBUG_PRINT("ymd_i: %s, sid1: %s, articles: %d" % (ymd_i, section_id_1, len(article_urls)))

    # 발견된 기사 URL 각각에 대해서 본문 가져오기
    for idx, article_url in enumerate(article_urls):
    #{
        (oid, aid) = get_oid_aid_from_url(article_url)
        uid = oid + "." + aid

        # 이미 crawl 한 것을 중복해서 다시 crawl 하지 않는다.
        if uid in uids_crawled:
            DEBUG_PRINT("already crawled: %s" % (uid))
            continue

        if ((idx+1) % 10) == 0: time.sleep(0.1)
        elif ((idx+1) % 50) == 0: time.sleep(0.1)

        DEBUG_PRINT("article_url: %s" % (article_url))

        html = get_url(article_url)

        if len(html) < 100:
            time.sleep(1.0)
            html = get_url(url)

        #encoding = "utf-8"
        #for idx, line in enumerate(html.split("\n")):
        #    if idx >= 100: break
        #    if line.find("<meta charset=\"euc-kr\">") >= 0: # 인코딩이 euc-kr 이면, utf-8 로 변환 필요
        #        encoding = "euc-kr"
        #        break

        og_meta = {}
        # og:description, og:article:author, og:url, og:title, og:type, og:image

        article_input_date = ''

        for idx, line in enumerate(html.split("\n")):
            if line.find("og:description") > 0:
                og_meta["description"] = meta_extract_content("og:description", line)
                #if encoding == "euc-kr":
                #    og_meta["description"] = og_meta["description"].decode("euc-kr").encode("utf-8")
                og_meta["description"] = strip_useless_chars(og_meta["description"])
            if line.find("og:url") > 0:
                og_meta["url"] = meta_extract_content("og:url", line)
            if line.find("og:article:author") > 0:
                og_meta["author"] = meta_extract_content("og:article:author", line)
                #if encoding == "euc-kr":
                #    og_meta["author"] = og_meta["author"].decode("euc-kr").encode("utf-8")
                og_meta["author"] = og_meta["author"].replace(" | 네이버", "")
                og_meta["author"] = strip_useless_chars(og_meta["author"])
 
            if line.find("og:title") > 0:
                og_meta["title"] = meta_extract_content("og:title", line)
                #if encoding == "euc-kr":
                #    # og_meta["title"] = og_meta["title"].decode("euc-kr").encode("utf-8")
                og_meta["title"] = strip_useless_chars(og_meta["title"])
            if line.find("og:type") > 0:
                og_meta["type"] = meta_extract_content("og:type", line)
            if line.find("og:image") > 0:
                og_meta["image"] = meta_extract_content("og:image", line)

            # 기사입력 시간이 틀린 기사는 제외 시킬 것
            # <span class="t11">2019.08.09. 오전 2:00</span>
            if line.find("기사입력") >= 0 and line.find("</span>") >= 0:
                p = line.find(">")
                if p > 0:
                    article_input_date = line[p+1:]
                    q = article_input_date.find("<")
                    if q > 0:
                        article_input_date = article_input_date[0:q]
                        article_input_date = article_input_date.replace(".", "")
                        article_input_date = article_input_date.replace(" ", "")
                        if len(article_input_date) >= 8:
                            article_input_date = article_input_date[0:8]
                        else:
                            article_input_date = ''
                    else:
                        article_input_date = ''
             

        DEBUG_PRINT("== article_input_date: %s" % (article_input_date))
        DEBUG_PRINT("== ymd_i: %s" % (ymd_i))

        # 기사입력 시간이 틀린 기사는 제외 시킬 것 => 미래의 날짜로 나오는 경우가 있음.
        if article_input_date != "" and article_input_date != ymd_i:
            DEBUG_PRINT("-ERROR WRONG DATE article_input_date:%s, ymd_i:%s" % (article_input_date, ymd_i))
            continue

        for key in og_meta.keys():
            DEBUG_PRINT("og_meta[%s]: [%s]" % (key, og_meta[key]))

        body_text = ''
        # 본문은 <div id=articleBodyContents> or <div id=newsEndContents> 와 </div> 사이에 텍스트로 있다.

        body_begin = False
        for line in html.split("\n"):
            #if encoding == "euc-kr":
            #    old_line = line
            #    line = line.decode("euc-kr").encode("utf-8")
            #    if len(old_line) > len(line)+100:
            #        DEBUG_PRINT("old_line(%d): line:(%d) %s" % (len(old_line), len(line), line))

            # if line.find("기사입력") >= 0:
            #     DEBUG_PRINT("line: %s" % (line))

            if line.find("id=\"articleBodyContents\"") >= 0:
                body_begin = True
                continue

            if line.find("class=\"news_end\"") >= 0:
                body_begin = True
                continue

            if body_begin and line.strip() == "</div>":
                body_begin = False
                break

            if body_begin:
                body_text += line

        DEBUG_PRINT("-")

        # if encoding == "euc-kr":
        #     body_text = body_text.decode("euc-kr").encode("utf-8")
        body_text = strip_useless_chars(body_text)
        body_text = strip_useless_tags(body_text)

        if body_text == "":
            body_text = og_meta["description"]

        # DEBUG_PRINT("body_text: (%d) %s" % (len(body_text), body_text[0:80]))
        old_body = body_text

        if len(body_text) < 10:
            DEBUG_PRINT("old_body: %s" % (old_body))
            DEBUG_PRINT("prob_url: %s"  % (article_url))

        DEBUG_PRINT("%s, %d/%d, body_text: (%d) %s" % (section_name, idx, len(article_urls), len(body_text), body_text[0:256]))

        fp.write("ARTICLE, %s, %s, %s, %s, %s, %s, %s\n" % (ymd_i, section_name, article_url, uid, og_meta["author"], og_meta["title"], body_text))

        comment_list = get_comments(article_url)
        for comment in comment_list:
            fp.write("COMMENT, %s, %s, %s\n" % (uid, comment[0], comment[1]))

        # DEBUG_PRINT("body_text: %s" % (body_text))

        uids_crawled[uid] = True
    #}
#}


def crawl_naver_news_date(ymd_i, output_csv):
#{
    # 기존에 이미 crawl한 url은 중복해서 crawl하지 않는다.
    uids_crawled = {}
    if os.path.exists(output_csv):
        fp = open(output_csv, "r")
        for line in fp:
            if line.find("ARTICLE") >= 0:
                row = line.split(",")
                # [0]ARTICLE, [1]DATE, [2]SECTION, [3]URL, [4]UID, [5]SOURCE, [6]TITLE, [7] TEXT
                uid = row[4].strip()
                uids_crawled[uid] = True
        fp.close()

    fp = open(output_csv, "a+")

    articles = []

    sections = {"100":"정치","101":"경제","102":"사회","103":"생활/문화","104":"세계","105":"IT/과학","110":"오피니언"}

    for sid1 in sections.keys():
        crawl_naver_news_date_section(ymd_i, sid1, sections[sid1], fp, uids_crawled)

    fp.close()
#}


def yyyymmddhhmmss_2_time(h):
#{
    ts = [int(h[0:4]), int(h[4:6]), int(h[6:8]), int(h[8:10]), int(h[10:12]),
        int(h[12:14]), 0, 0, -1]
    return time.mktime(ts)
#}

def time_2_yyyymmddhhmmss(t):
#{
    return time.strftime("%Y%m%d%H%M%S", time.localtime(int(t)))
#}

def next_date(ymd):
#{
    t = yyyymmddhhmmss_2_time(ymd[0:8]+"000000")
    return time_2_yyyymmddhhmmss(t+24*3600)[0:8]
#}

if __name__ == "__main__":
#{
    if len(sys.argv) < 2:
    #{
        print("Usage: %s yyyymmdd" % (sys.argv[0]))
        sys.exit(0)
    #}

    os.umask(0)

    ymd = sys.argv[1] # 날짜
    output_csv = ymd + ".csv"

    # 기존에 파일이 존재하면, 해당 파일에 이미 crawl한 것을 유지하고, 그 뒤부터 처리하도록 바꾼다
    # if os.path.exists(output_csv):
    #     os.remove(output_csv)

    crawl_naver_news_date(ymd, output_csv)
#}
