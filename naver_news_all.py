#!/bin/env python
#-*- encoding: utf-8 -*-

import time, sys, os

def DEBUG_PRINT(str_line):
#{
    import inspect
    print("(%s,%d) %s" % (__file__, inspect.stack()[1][2], str_line.strip()))
#}

def yyyymmddhhmmss_2_time(h):
    ts = (int(h[0:4]), int(h[4:6]), int(h[6:8]), int(h[8:10]), int(h[10:12]),
        int(h[12:14]), 0, 0, -1)
    return time.mktime(ts)

def time_2_yyyymmddhhmmss(t):
    return time.strftime("%Y%m%d%H%M%S", time.localtime(int(t)))

def next_date(ymd):
#{
    t = yyyymmddhhmmss_2_time(ymd[0:8]+"000000")
    return time_2_yyyymmddhhmmss(t+24*3600)[0:8]
#}

def run_process(cmd_args):
#{
    import subprocess
    process = subprocess.Popen(cmd_args, stdout=subprocess.PIPE)
    out, err = process.communicate()
    return out
#}

def commands_getoutput(cmd_args):
#{
    import subprocess
    process = subprocess.Popen(cmd_args, stdout=subprocess.PIPE, shell=True)
    out, err = process.communicate()
    return out
#}

if __name__ == "__main__":
#{
    if len(sys.argv) < 2:
        print("Usage: %s 20170102 20171231" % (sys.argv[0]))
        sys.exit(0)

    ymd1 = sys.argv[1]
    ymd2 = sys.argv[2]

    ymd_i = ymd1
    while ymd_i <= ymd2:
        cmd = "python naver_news.py %s" % (ymd_i)

        DEBUG_PRINT("cmd: %s" % (cmd))

        output = run_process(cmd.split(" "))
        DEBUG_PRINT("output: %s" % (output))

        ymd_i = next_date(ymd_i)
#}
